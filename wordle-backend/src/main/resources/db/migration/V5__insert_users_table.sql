CREATE TABLE users (
                       email VARCHAR(255) NOT NULL,
                       user_login VARCHAR(50) UNIQUE NOT NULL,
                       country VARCHAR(50),
                       firstname VARCHAR(50),
                       lastname VARCHAR(50),
                       PRIMARY KEY (email)
);

DELETE FROM games;

ALTER TABLE games
    ADD CONSTRAINT fk_user_login FOREIGN KEY (user_login) REFERENCES users(user_login);

ALTER TABLE games
    ALTER COLUMN user_login DROP NOT NULL;