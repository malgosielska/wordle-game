-- Tworzenie tabeli 'words'
CREATE TABLE words (
                       word VARCHAR(255) PRIMARY KEY,
                       difficulty_level VARCHAR(50) NOT NULL
);

-- Tworzenie tabeli 'games'
CREATE TABLE games (
                       game_id SERIAL PRIMARY KEY,
                       userLogin VARCHAR(50) NOT NULL,
                       word VARCHAR(255),
                       game_status VARCHAR(10) NOT NULL,
                       tries INT NOT NULL,
                       FOREIGN KEY (word) REFERENCES words(word)
);