package com.example.wordlebackend.controller;

import com.example.wordlebackend.service.StatsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
@RequestMapping("/stats")
public class StatsController {
    @Autowired
    private StatsService gameStatsService;

    @GetMapping("/{userLogin}")
    public ResponseEntity<Map<String, Long>> getUserStats(@PathVariable String userLogin) {
        Map<String, Long> stats = gameStatsService.getUserStats(userLogin);
        return ResponseEntity.ok(stats);
    }
}
