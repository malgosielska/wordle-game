package com.example.wordlebackend.controller;

import com.example.wordlebackend.model.Game;
import com.example.wordlebackend.model.GuessResponse;
import com.example.wordlebackend.service.GameService;
import com.example.wordlebackend.service.WordsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class WordleController {

    private final GameService gameService;
    private final WordsService wordsService;

    @Autowired
    public WordleController(GameService gameService, WordsService wordsService) {
        this.gameService = gameService;
        this.wordsService = wordsService;
    }

    @GetMapping("/startGame")
    public String startGame(@RequestParam String userLogin) {
        String word = wordsService.getRandomWord().getWord();
        System.out.println(word);
        return gameService.addGameToDataStore(word, userLogin);
    }

    @PostMapping("/submitGuess")
    public GuessResponse submitGuess(@RequestParam String guess, @RequestParam String userToken) {
        return gameService.submitGuess(guess, userToken);
    }

    @GetMapping("/getUserGames")
    public List<Game> getUserGames(@RequestParam String userLogin) {
        return gameService.getGamesByUserLogin(userLogin);
    }

}
