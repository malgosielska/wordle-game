package com.example.wordlebackend.service;

import com.example.wordlebackend.model.Game;
import com.example.wordlebackend.model.GameStatus;
import com.example.wordlebackend.repository.GameRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class StatsService {
    private final GameRepository gameRepository;

    @Autowired
    public StatsService(GameRepository gameRepository) {
        this.gameRepository = gameRepository;
    }

    public Map<String, Long> getUserStats(String userLogin) {
        Map<String, Long> stats = new HashMap<>();

        // Pobranie wszystkich gier danego użytkownika
        List<Game> userGames = gameRepository.findByUserLogin(userLogin);

        long totalWins = 0;
        long totalLosses = 0;
        Map<Integer, Long> winsInTries = new HashMap<>();

        // Przetwarzanie gier
        for (Game game : userGames) {
            if (game.getGameStatus() == GameStatus.WIN) {
                totalWins++;
                int tries = game.getTries();
                winsInTries.put(tries, winsInTries.getOrDefault(tries, 0L) + 1);
            } else {
                totalLosses++;
            }
        }

        // Dodanie statystyk do mapy
        stats.put("totalWins", totalWins);
        stats.put("totalLosses", totalLosses);

        for (int i = 1; i <= Game.MAX_TRIES; i++) {
            stats.put("winsIn" + i + "Tries", winsInTries.getOrDefault(i, 0L));
        }

        return stats;
    }
}
