package com.example.wordlebackend.service;

import com.example.wordlebackend.model.Word;
import com.example.wordlebackend.repository.WordRepository;
import jakarta.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Random;

import static org.springframework.beans.factory.config.ConfigurableBeanFactory.SCOPE_SINGLETON;

@Scope(scopeName = SCOPE_SINGLETON)
@Service
public class WordsService {
    private final WordRepository wordRepository;
    private List<Word> words;

    @Autowired
    public WordsService(WordRepository wordRepository) {
        this.wordRepository = wordRepository;
    }

    @PostConstruct
    private void loadWordsFromDatabase() {
        words = wordRepository.findAll();
    }

    public Word getRandomWord() {
        if (words == null || words.isEmpty()) {
            return null;
        }
        Random random = new Random();
        return words.get(random.nextInt(words.size()));
    }

    public List<Word> getWords() {
        return words;
    }

    public void setWords(List<Word> words) {
        this.words = words;
    }
}
