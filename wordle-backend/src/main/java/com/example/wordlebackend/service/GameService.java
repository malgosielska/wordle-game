package com.example.wordlebackend.service;

import com.example.wordlebackend.model.*;
import com.example.wordlebackend.repository.GameRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

import static org.springframework.beans.factory.config.ConfigurableBeanFactory.SCOPE_SINGLETON;

@Scope(scopeName = SCOPE_SINGLETON)
@Service
public class GameService {

    ConcurrentHashMap<String, Game> userGames = new ConcurrentHashMap<>();
    GameRepository gameRepository;
    private static final Logger logger = LoggerFactory.getLogger(GameService.class);

    @Autowired
    public GameService(GameRepository gameRepository) {
        this.gameRepository = gameRepository;
    }

    public String addGameToDataStore(String word, String userLogin) {
        String userKey = UUID.randomUUID().toString();
        System.out.println(userLogin);
        Game game = new Game(word, userLogin, 0, GameStatus.IN_PROGRESS);
        userGames.put(userKey, game);
        game.setUserLogin(userLogin); // Ustawienie userLogin
        game.setGameId(100L); // Ustawienie userLogin
        return userKey;
    }

    private List<Letter> getLetterStatus(String solutionWord, String requestWord) {
        List<Letter> letterStatusList = new ArrayList<>();

        for (int i = 0; i < solutionWord.length(); i++) {
            char solutionChar = Character.toUpperCase(solutionWord.charAt(i));
            char requestChar = Character.toUpperCase(requestWord.charAt(i));

            if (solutionChar == requestChar) {
                letterStatusList.add(new Letter(requestChar, CharacterValue.CORRECT));
            } else if (solutionWord.toUpperCase().contains(Character.toString(requestChar))) {
                letterStatusList.add(new Letter(requestChar, CharacterValue.PRESENT_BUT_MISPLACED));
            } else {
                letterStatusList.add(new Letter(requestChar, CharacterValue.NOT_PRESENT));
            }
        }

        return letterStatusList;
    }

    public GuessResponse submitGuess(String requestWord, String userToken) {
        Game userGame = Optional.ofNullable(userGames.get(userToken))
                .orElseThrow(() -> new RuntimeException("Session does not exist."));
        int currentTry = userGame.getTries();
        userGame.setTries(++currentTry);
        String solutionWord = userGame.getWord();

        List<Letter> letterStatusList = getLetterStatus(solutionWord, requestWord);

        if (userGame.getTries() >= Game.MAX_TRIES) {
            userGames.remove(userToken);
            return handleGameOver(userGame, currentTry, requestWord, solutionWord, letterStatusList);
        } else if (solutionWord.equalsIgnoreCase(requestWord)) {
            userGame.setGameStatus(GameStatus.WIN);
            userGame.setTries(currentTry);
            saveGame(userGame);
            return new GuessResponse(currentTry, requestWord, GameStatus.WIN, letterStatusList);
        } else {
            userGames.replace(userToken, userGame);
            return new GuessResponse(currentTry, "", GameStatus.IN_PROGRESS, letterStatusList);
        }
    }

    private GuessResponse handleGameOver(Game userGame, int currentTry, String requestWord, String solutionWord, List<Letter> letterStatusList) {
        if (solutionWord.equalsIgnoreCase(requestWord)) {
            userGame.setGameStatus(GameStatus.WIN);
            userGame.setTries(currentTry);
            saveGame(userGame);
            return new GuessResponse(currentTry, requestWord, GameStatus.WIN, letterStatusList);
        } else {
            userGame.setGameStatus(GameStatus.LOSE);
            userGame.setTries(currentTry);
            saveGame(userGame);
            return new GuessResponse(currentTry, solutionWord, GameStatus.LOSE, letterStatusList);
        }
    }

    private void saveGame(Game game) {
        try {
            gameRepository.save(game);
        } catch (Exception e) {
            logger.error("Error while saving game to the database", e);
        }
    }

    public List<Game> getGamesByUserLogin(String userLogin) {
        try {
            return gameRepository.findByUserLogin(userLogin);
        } catch (Exception e) {
            logger.error("Error while reading data from database", e);
        }
        return new ArrayList<>();
    }
}
