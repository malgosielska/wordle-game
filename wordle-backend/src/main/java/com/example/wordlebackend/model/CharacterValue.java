package com.example.wordlebackend.model;

public enum CharacterValue {
    NOT_PRESENT,
    PRESENT_BUT_MISPLACED,
    CORRECT
}
