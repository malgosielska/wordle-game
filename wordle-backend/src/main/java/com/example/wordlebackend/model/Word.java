package com.example.wordlebackend.model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@Entity
@Table(name = "words")
public class Word {
    @Id
    @Column(name = "word", length = 255)
    private String word;
    @Column(name = "difficulty_level", nullable = false, length = 50)
    private String difficultyLevel;

    public Word(String word, String difficultyLevel) {
        this.word = word;
        this.difficultyLevel = difficultyLevel;
    }

    public Word() {

    }

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }

    public String getDifficultyLevel() {
        return difficultyLevel;
    }

    public void setDifficultyLevel(String difficultyLevel) {
        this.difficultyLevel = difficultyLevel;
    }
}