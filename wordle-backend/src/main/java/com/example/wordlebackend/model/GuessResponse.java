package com.example.wordlebackend.model;

import java.util.List;

public class GuessResponse {

    private int currentTry;
    private String word;
    private GameStatus status;
    private List<Letter> letters;

    public GuessResponse(int currentTry, String word, GameStatus status, List<Letter> letters) {
        this.currentTry = currentTry;
        this.word = word;
        this.status = status;
        this.letters = letters;
    }

    public List<Letter> getLetters() {
        return letters;
    }

    public void setLetters(List<Letter> letters) {
        this.letters = letters;
    }

    public GuessResponse() {
    }

    public GameStatus getStatus() {
        return status;
    }

    public void setStatus(GameStatus status) {
        this.status = status;
    }

    public int getCurrentTry() {
        return currentTry;
    }

    public void setCurrentTry(int currentTry) {
        this.currentTry = currentTry;
    }

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }

}
