package com.example.wordlebackend.model;

public class Letter {
    private char letter;
    private CharacterValue value;

    public Letter(char letter, CharacterValue value) {
        this.letter = letter;
        this.value = value;
    }

    public char getLetter() {
        return letter;
    }

    public void setLetter(char letter) {
        this.letter = letter;
    }

    public CharacterValue getValue() {
        return value;
    }

    public void setValue(CharacterValue value) {
        this.value = value;
    }
}
