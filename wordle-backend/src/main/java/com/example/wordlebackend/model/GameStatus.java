package com.example.wordlebackend.model;

public enum GameStatus {
    WIN, LOSE, IN_PROGRESS
}
