package com.example.wordlebackend.model;

import jakarta.persistence.*;

@Entity
@Table(name = "games")
public class Game {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "game_id")
    private Long gameId;

    @Column(name = "userLogin", nullable = false, length = 50)
    private String userLogin;

    @Column(name = "word", length = 255)
    private String word;

    @Column(name = "tries", nullable = false)
    private int tries;

    @Enumerated(EnumType.STRING)
    @Column(name = "game_status", nullable = false, length = 10)

    private GameStatus gameStatus;

    public static final int MAX_TRIES = 5;

    public Game() {
    }

    public Game(String word, String userLogin, int tries, GameStatus gameStatus) {
        this.word = word;
        this.userLogin = userLogin;
        this.tries = tries;
        this.gameStatus = gameStatus;
    }

    public Long getGameId() {
        return gameId;
    }

    public void setGameId(Long id) {
        this.gameId = id;
    }

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }

    public String getUserLogin() {
        return userLogin;
    }

    public void setUserLogin(String userLogin) {
        this.userLogin = userLogin;
    }

    public int getTries() {
        return tries;
    }

    public void setTries(int currentTries) {
        this.tries = currentTries;
    }

    public GameStatus getGameStatus() {
        return gameStatus;
    }

    public void setGameStatus(GameStatus gameStatus) {
        this.gameStatus = gameStatus;
    }

    @Override
    public String toString() {
        return "Game{" +
                "gameId=" + gameId +
                ", word='" + word + '\'' +
                ", userLogin='" + userLogin + '\'' +
                ", tries=" + tries +
                ", gameStatus=" + gameStatus +
                '}';
    }
}
