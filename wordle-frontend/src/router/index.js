import { createRouter, createWebHistory } from 'vue-router';
import Game from '@/components/game/Game.vue';
import Stats from '@/components/stats/Stats.vue';

const routes = [
    { path: '/', redirect: '/startGame' },
    { path: '/startGame', component: Game },
    { path: '/stats', component: Stats }
];

const router = createRouter({
    history: createWebHistory(),
    routes
});

export default router;
